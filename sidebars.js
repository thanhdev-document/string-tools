module.exports = {
  someSidebar: {
    Migrating: ['migrating', 'changelog'],
    Installation: ['installation', 'quickview'],
    Editing: ['pages', 'components', 'rtl'],
    'Server Configurations': ['server-configurations'],
    Deploy: ['build-and-deploy'],
  },
}
