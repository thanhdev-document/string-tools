---
title: Pages
description:
---

## Introduction

### Directory

All pages in the "src/views" directory:

![Pages 1](/img/pages/1.png)

We already know that there are 24 functions and 24 pages corresponding to those, so we have 24 files that represent them and one file for index page.

### Routing

Open **functions.js** file in the "src/views" directory:

![Pages 2](/img/pages/2.png)

You can see a list that contains all function's information here, that's an array of 24 elements, each element has 3 properties:

- name: Function's name
- component: Component's name, it corresponds to the name of the file in "src/views", if you change it, you must change the corresponding file name.
- link: Corresponding path of each function.

You can change the **"name"** and **"link"** properies, the homepage page will automatically change according to, for example:

![Pages 3](/img/pages/3.png)

Before:

![Pages 4](/img/pages/4_1.png)

and

![Pages 4 2](/img/pages/4_2.png)

After:

![Pages 5](/img/pages/5_1.png)

and

![Pages 5 2](/img/pages/5_2.png)

## Index Page

Open file **"index.vue"** inside the **"src/views"** directory:

![Pages 6](/img/pages/6.png)

Looking at it, you can also understand the mechanism: file **functions.js** will be imported and will be rendered in **v-card** tag

The card looks like this:

![Pages 6](/img/pages/6_1.png)

```html
<v-flex xs4 v-for="(fnc, key) in functions" :key="key">
  <v-card>
    <v-card-title primary-title>
      <div>
        <h3 class="headline mb-0">{{fnc.name}}</h3>
      </div>
    </v-card-title>
    <v-card-actions>
      <v-btn flat color="orange" @click="$router.push(fnc.link)">Use this tool</v-btn>
    </v-card-actions>
  </v-card>
</v-flex>
```

**v-flex** tag is responsible for dividing the column, each row has 12 columns, the **"xs4"** attribute means taking 4 columns for a card, you can change it as an option, for example if changed to **"xs3"** each column will have 4 cards.

Take a look at [grid system](https://v15.vuetifyjs.com/en/framework/grid) here

About **v-card** element, you should see details about it [here](https://v15.vuetifyjs.com/en/components/cards).

## Function Page

All the function pages are in the **"src/views"** folder, All have the same structure so I will mention a specific case to be an example.

Open the **"sort-lines.vue"** file:

![Pages 7](/img/pages/7.png)

On site:

![Pages 7](/img/pages/7_1.png)

You can see **v-card** element consists of 2 parts: **v-card-title** and **v-card-content**. Feel free to change h1 element's value, **v-textarea** placeholder, **v-btn** color or value,... but you mustn't change but you cannot change the value in **"@click"**.

If you are wondering where the copy and download buttons are, it is included in a separate component called **"actions"** for reuse.

Next, we'll discovery what is [**"actions"** component](/docs/components).
