---
title: Installation
description: Installation
slug: /
---

:::tip

If you have purchased this product before (v1.2.1), please go to [Migrating](/docs/migrating) site.

:::

## Unzip the downloaded file

After upzipping the product file, you'll see 2 folders:

![Upzip 1](/img/installation/1.png)

- string-tools: product folder

![Upzip 1](/img/installation/unzip_1.png)

- v12.18.3: Node.js folder

![Nodejs](/img/installation/node_1.png)

## Test Node.js

Open the Node.js folder:

![Nodejs](/img/installation/node_1.png)

Press Shift + Right Click to open Powershell:

![Nodejs 2](/img/installation/node_2.png)

Run this command to verify that Nodejs working:

```
./node -v
```

![Nodejs 2](/img/installation/node_3.png)

## Install VSCode (Optional)

Visual Studio Code (easier for editting)

[Install here](https://code.visualstudio.com/download)

[How to install](https://www.thewindowsclub.com/visual-studio-code-download)

## Run the site on localhost

Command:

```
../v12.18.3/npm run serve
```

![Serve](/img/installation/serve.gif)
