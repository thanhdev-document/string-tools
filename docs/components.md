---
title: Components
description:
---

## Actions Component

Actions component is the only component in here.

Open file: **"src/components/Actions.vue"**

![Components 1](/img/components/1.png)

If you take a look at the [Component API Overview](https://v15.vuetifyjs.com/en/components/api-explorer) that I mentioned in [Quickview](/docs/quickview) and finished reading [Pages](/docs/pages), you will already see many familiar elements in this, only the new element is a snackbar, it will appear when you press the copy button.

![Components 2](/img/components/2.png)

Its default is the bottom, you can change it by adding the following attributes:

![Components 3](/img/components/3.png)

![Components 4](/img/components/4.png)

Read about [Snackbar Component](https://v15.vuetifyjs.com/en/components/snackbars)
