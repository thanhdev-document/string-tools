---
title: Migrating to v1.2.2
description: Migrating to v1.2.2
---
## Migrating from v1.2.1 to v1.2.2

Open folder "**v1.2.2 patch**":

![](/img/migrating/v122_1.png)

and copy folder "**views**":

![](/img/migrating/v122_2.png)

Then go to your v1.2.1 source code folder, open "**src**" folder and paste it there:

![](/img/migrating/v122_3.png)

![](/img/migrating/v122_4.png)

After finishing, [BUILD](/docs/build-and-deploy) it!

## Migrating from v1.2.0 to v1.2.1

This update does not change the code, just add the ["Server Configurations"](/docs/server-configurations).

## Migrating from v1.1.0 to v1.2.0

Migrating from v1.1.0 to v1.2.0

File location: "src/views/url-encode-decode.vue"

After finishing, [BUILD](/docs/build-and-deploy) it!

### Video tutorial

<video controls style={{objectFit: 'fill', width: '100%', height: '100%'}}>

  <source src="/img/migrating/migrating_video.mp4" type="video/mp4" />
  Your browser does not support the video tag.
</video>

## Code

### First part

```html
<v-btn :color="separate ? 'primary' : 'red'" dark @click="separate = !separate">
  Separate line:
  <v-icon right dark>{{ separate ? 'done' : 'clear' }}</v-icon>
</v-btn>
```

### Second part

```jsx
<script>
export default {
  data: () => ({
    text: '',
    result: '',
    separate: false
  }),
  methods: {
    encode() {
      if (this.separate) {
        const text = this.text.split('\n')
        this.result = text.map((val) => encodeURIComponent(val)).join('\n')
        return
      }
      this.result = encodeURIComponent(this.text)
    },
    decode() {
      if (!this.separate) {
        const text = this.text.replace(/\n|\r/g, '')
        this.result = decodeURIComponent(text)
        return
      }
      this.result = decodeURIComponent(this.text)
    }
  }
}
</script>
```
