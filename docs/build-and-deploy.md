---
title: Build & Deploy
description: Migrating to v1.2.0
---

## Build

Command:

```
../v12.18.3/npm run build
```

<video controls style={{objectFit: 'fill', width: '100%', height: '100%'}}>

  <source src="/img/deploy/build.mp4" type="video/mp4" />
  Your browser does not support the video tag.
</video>

## Deploy

When the build is complete, you will see the "dist" folder created:

![Deploy 1](/img/deploy/1.png)

You only need to upload all files in the "dist" folder to your hosting.
