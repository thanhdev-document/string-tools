---
title: Enable RTL Support
description:
---

## Enable RTL Support

Open file App.vue inside "src" directory:

![RTL](/img/quickview/4.png)

Change **rtl: false** into **rtl: true**.
